import React from 'react'
import './Display.css'

const display = (props) => {
    return (
        <div className='display'>
            <div className='display-size'>{props.display}</div>
            <div className='evaluation-size'>
                {props.evaluation.toString() !== props.display.toString() ? props.evaluation : ' '}
            </div>
        </div>)
}

export default display