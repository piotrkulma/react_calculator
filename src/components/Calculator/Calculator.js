import {React, Component} from 'react'
import './Calculator.css'

import Buttons from './Buttons/Buttons'
import Display from './Display/Display'

class calculator extends Component {
    state = {
        memory: 0,
        display: '',
        evaluation: '',
        lastLabel: null
    }

    buttonClickHandler = (label) => {
        try {
            const evaluation = eval(this.state.display + label)
            this.setState({evaluation: evaluation})
        } catch (error) {

        }
        this.setState({display: this.state.display + label, lastLabel: label})
    }

    render() {
        return (
            <div className='calculator'>
                <Display display={this.state.display} evaluation={this.state.evaluation}/>
                <Buttons buttonClick={this.buttonClickHandler}/>
            </div>
        )
    }
}

export default calculator