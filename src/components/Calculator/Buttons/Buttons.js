import React from 'react'
import Button from './Button/Button'
import './Buttons.css'

const buttons = (props) => {
    return (
        <div className='buttons'>
            <Button type='operation' buttonClick={props.buttonClick} label='C'/>
            <Button type='number' buttonClick={props.buttonClick} label='7'/>
            <Button type='number' buttonClick={props.buttonClick} label='4'/>
            <Button type='number' buttonClick={props.buttonClick} label='1'/>
            <Button type='number' buttonClick={props.buttonClick} label='%'/>

            <Button type='operation' buttonClick={props.buttonClick} label='/'/>
            <Button type='number' buttonClick={props.buttonClick} label='8'/>
            <Button type='number' buttonClick={props.buttonClick} label='5'/>
            <Button type='number' buttonClick={props.buttonClick} label='2'/>
            <Button type='number' buttonClick={props.buttonClick} label='0'/>

            <Button type='operation' buttonClick={props.buttonClick} label='*'/>
            <Button type='number' buttonClick={props.buttonClick} label='9'/>
            <Button type='number' buttonClick={props.buttonClick} label='6'/>
            <Button type='number' buttonClick={props.buttonClick} label='3'/>
            <Button type='number' buttonClick={props.buttonClick} label='.'/>

            <Button type='operation' buttonClick={props.buttonClick} label='BCK'/>
            <Button type='operation' buttonClick={props.buttonClick} label='-'/>
            <Button type='operation' buttonClick={props.buttonClick} label='+'/>
            <Button type='operation' buttonClick={props.buttonClick} label='='/>

        </div>
    )
}

export default buttons