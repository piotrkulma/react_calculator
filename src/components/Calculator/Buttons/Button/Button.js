import React from 'react'
import './Button.css'

const button = (props) => {
    let classes = 'button'

    if (props.type !== undefined) {
        if (props.type === 'operation') {
            classes += " operation";

            if (props.label === '=') {
                classes += " big";
            }
        } else if (props.type === 'number') {
            classes += ' number';
        }
    }

    return (
        <div onClick={() => props.buttonClick(props.label)} className={classes}>{props.label}</div>
    )
}

export default button